var fs = require('fs')
var express = require('express')
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var winston = require('winston')
var models = require('./models/model.js')

// Create the express app
var app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))

// Parse the configuration file
var configFile = __dirname + '/config/config.json'
var configuration = JSON.parse(
  fs.readFileSync(configFile)
)

// Make the authentication string. No username ans pwd required in localhost
if (configuration.user && configuration.password)
  auth_str = configuration.user + ':' + configuration.password + '@'
else
  auth_str = ''

// Connect to mongo database
var mongoUrl = 'mongodb://' + auth_str + configuration.host + ':' + configuration.port + '/' + configuration.database
var conVariable = mongoose.createConnection(mongoUrl, { server: { poolSize: 4 }})

// Define the mongo data model
var mongoDataModel = conVariable.model('Data', new mongoose.Schema(models.model), 'data')

// Add the log file
winston.add(winston.transports.File, { filename: __dirname + '/log/logfile.log' })

// Main page
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/pages/index.html')
})

// Get data
app.get('/data', function (req, res) {
  mongoDataModel.find({}).sort({date: 1}).exec(function (err, data) {
    if (err) {
      winston.log('error', err)
      return res.send(err)
    }

    else
      return res.send(data)
  })
})

// Add data
app.post('/data', function (req, res) {
  // parse the data
  var data = {}
  data.amount = req.body.amount
  data.category = req.body.category
  data.date = req.body.date
  data.type = req.body.type

  // add the data in database
  var dataToSave = conVariable.model('Data', mongoDataModel, 'data')
  var dSave = new dataToSave(data)
  dSave.save(function (err) {
    if (err) {
      winston.log('error', err)
      return res.send(false)
    } else {
      winston.log('info', 'Added data')
      return res.send(true)
    }
  })
})

// Start the app on port = 3000
app.listen(8080, function () {
  console.log('App on 8080!')
})
