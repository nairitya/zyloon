module.exports = {
  model: {
    date: {
      type: Date,
      required: true
    },

    category: {
      type: String
    },

    type: {
      type: String
    },

    amount: {
      type: Number,
      required: true
    }
  }
}
